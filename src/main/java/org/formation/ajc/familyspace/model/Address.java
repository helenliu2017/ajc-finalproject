package org.formation.ajc.familyspace.model;

import javax.persistence.Embeddable;

@Embeddable
public class Address {

	private int number;
	private String road;
	private int postalCode;
	private String city;
	
	public Address() {
		super();
	}

	
	/**
	 * @param number
	 * @param road
	 * @param postalCode
	 * @param city
	 */
	public Address(int number, String road, int postalCode, String city) {
		super();
		this.number = number;
		this.road = road;
		this.postalCode = postalCode;
		this.city = city;
	}


	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}


	@Override
	public String toString() {
		return "Address [number=" + number + ", road=" + road + ", postalCode=" + postalCode + ", city=" + city + "]";
	}
	
	


	
	
}
