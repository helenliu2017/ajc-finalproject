package org.formation.ajc.familyspace.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Family {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToMany(mappedBy = "family", cascade = CascadeType.ALL)
	private Set<Parent> parents = new HashSet<>();

	@OneToMany(mappedBy = "family", cascade = CascadeType.ALL)
	private Set<Child> children = new HashSet<>();
	private Address address;

	@Embedded
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Parent> getParents() {
		return parents;
	}

	public void setParents(Set<Parent> parents) {
		this.parents = parents;
	}

	public Set<Child> getChildren() {
		return children;
	}

	public void setChildren(Set<Child> children) {
		this.children = children;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void addParent(Parent parent) {
		parent.setFamily(this);
		this.parents.add(parent);
	}

	public void addChild(Child child) {
		child.setFamily(this);
		this.children.add(child);
	}
	
	public Parent getFather() {
		return parents.stream().filter(p -> org.formation.ajc.familyspace.model.Person.Gender.MASCULIN == p.getGender()).findFirst().orElse(null);
	}
	
	public Parent getMother() {
		return parents.stream().filter(p -> org.formation.ajc.familyspace.model.Person.Gender.FEMININ == p.getGender()).findFirst().orElse(null);
	}

}
