package org.formation.ajc.familyspace.controller;

import java.util.List;

import org.formation.ajc.familyspace.model.Family;
import org.formation.ajc.familyspace.service.FamilyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/families")
public class FamilyController {
	private static final Logger LOGGER = LoggerFactory.getLogger(FamilyController.class);
	@Autowired
	private FamilyService familyService;

	@GetMapping
	public ResponseEntity<List<Family>> getAllFamily() {
		List<Family> familyList = familyService.findAllFamily();
		LOGGER.info("Nombre de familles est : {}", familyList.size());
		return ResponseEntity.ok(familyList);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Family> getFamilyById(@RequestParam("id") Long id, Model model) {
		Family family = familyService.findByFamilyId(id);
		LOGGER.info("Famille trouvé est: {}", family.getId());
		return ResponseEntity.ok(family);
	}
}
