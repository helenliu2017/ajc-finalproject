package org.formation.ajc.familyspace;

import javax.transaction.Transactional;

import org.formation.ajc.familyspace.dao.FamilyDAO;
import org.formation.ajc.familyspace.model.Address;
import org.formation.ajc.familyspace.model.Child;
import org.formation.ajc.familyspace.model.Child.Section;
import org.formation.ajc.familyspace.model.Family;
import org.formation.ajc.familyspace.model.Parent;
import org.formation.ajc.familyspace.model.Person.Gender;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {
	private boolean alreadySetup = false;
	private final FamilyDAO familyDAO;

	public SetupDataLoader(FamilyDAO familyDAO) {
		super();
		this.familyDAO = familyDAO;
	}

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (alreadySetup) {
			return;
		}
		Address address = new Address(8, "Rue de Paris,", 75001, "Paris");
		for (int i = 0; i < 10; i++) {
			Family family = new Family();
			family.setAddress(address);

			Parent father = new Parent();
			father.setGender(Gender.MASCULIN);
			father.setFirstName("someMother-0" + i);
			father.setLastName("surname-a0" + i);
			father.setAge(20 + i);

			Parent mother = new Parent();
			mother.setGender(Gender.FEMININ);
			mother.setFirstName("someMother-0" + i);
			mother.setLastName("surname-a0" + i);
			mother.setAge(20 + i);

			family.addParent(father);
			family.addParent(mother);

			for (int j = 0; j < 3; j++) {
				Child child = new Child();
				child.setGender((int) (Math.random() + 0.5) == 0 ? Gender.FEMININ : Gender.MASCULIN);
				child.setFirstName("SomeChild - 0" + i);
				child.setLastName("SURNAME - B0" + i);
				child.setAge(j + 1);
				switch (j) {
				case 0:
					child.setSection(Section.PETITE);
					break;
				case 1:
					child.setSection(Section.MOYENNE);
					break;
				case 2:
					child.setSection(Section.GRANDE);
					break;
				}
				family.addChild(child);
			}
			familyDAO.save(family);
		}
		alreadySetup = true;

	}
}
