package org.formation.ajc.familyspace.dto;

import java.util.Set;

import org.formation.ajc.familyspace.model.Address;
import org.formation.ajc.familyspace.model.Child;
import org.formation.ajc.familyspace.model.Parent;

public class FamilyDetailDTO {

	private Long id;
	private Parent father;
	private Parent mother;
	private Set<Child> children;
	private Address address;
	
	// Getters and Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Parent getFather() {
		return father;
	}

	public void setFather(Parent father) {
		this.father = father;
	}

	public Parent getMother() {
		return mother;
	}

	public void setMother(Parent mother) {
		this.mother = mother;
	}

	public Set<Child> getChildren() {
		return children;
	}

	public void setChildren(Set<Child> children) {
		this.children = children;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
