package org.formation.ajc.familyspace.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.formation.ajc.familyspace.dao.FamilyDAO;
import org.formation.ajc.familyspace.dto.FamilyDetailDTO;
import org.formation.ajc.familyspace.model.Child;
import org.formation.ajc.familyspace.model.Family;
import org.formation.ajc.familyspace.model.Parent;
import org.formation.ajc.familyspace.model.Person.Gender;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class FamilyService {

	public final FamilyDAO familyDAO;

	public FamilyService(FamilyDAO familyDAO) {
		super();
		this.familyDAO = familyDAO;
	}
	
	// Show a family in detail
	public Family findByFamilyId(final Long id) {
		Family family = familyDAO.findById(id).orElseThrow(EntityNotFoundException::new);
		family.getChildren().size(); // Activily load lazy-load set

		return family;
	}

	// Show the list of families
	public List<Family> findAllFamily() {
		return familyDAO.findAll();
	}

	// Update of CRUD
	private void updateParent(final Parent parentFrom, final Parent parentTo) {

		parentTo.setFirstName(parentFrom.getFirstName());
		parentTo.setLastName(parentFrom.getLastName());
		parentTo.setAge(parentFrom.getAge());
		parentTo.setTel(parentFrom.getTel());
		parentTo.setEmail(parentFrom.getEmail());
	}

	public Family updateFamily(final FamilyDetailDTO familyDetailDTO) {

		Family familyToUpdate = familyDAO.findById(familyDetailDTO.getId()).orElseThrow(EntityNotFoundException::new);

		updateParent(familyDetailDTO.getFather(), familyToUpdate.getFather());
		updateParent(familyDetailDTO.getMother(), familyToUpdate.getMother());
		familyToUpdate.setAddress(familyDetailDTO.getAddress());

		return familyToUpdate;
	}

	// Create of CRUD
	public Family addNewFamily(final FamilyDetailDTO familyDetailDTO) {

		Family newFamily = new Family();

		familyDetailDTO.getFather().setGender(Gender.MASCULIN);
		newFamily.addParent(familyDetailDTO.getFather());
		familyDetailDTO.getMother().setGender(Gender.FEMININ);
		newFamily.addParent(familyDetailDTO.getMother());
		newFamily.setAddress(familyDetailDTO.getAddress());
		newFamily.setChildren(familyDetailDTO.getChildren());

		return familyDAO.save(newFamily);
	}

	// Delete of CRUD
	public void deleteFamily(final long id) {
		familyDAO.deleteById(id);
	}

	// Add a child to a family
	public Family addChildToFamily(final Long familyId, final Child child) {
		Family familyToUpdate = familyDAO.findById(familyId).orElseThrow(EntityNotFoundException::new);
		familyToUpdate.addChild(child);

		return familyToUpdate;
	}

}
