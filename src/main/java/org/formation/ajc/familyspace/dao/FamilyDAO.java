package org.formation.ajc.familyspace.dao;

import org.formation.ajc.familyspace.model.Family;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyDAO extends JpaRepository<Family, Long> {

}
