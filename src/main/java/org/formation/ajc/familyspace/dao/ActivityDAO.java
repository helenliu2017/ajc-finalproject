package org.formation.ajc.familyspace.dao;

import org.formation.ajc.familyspace.model.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityDAO extends JpaRepository<Activity, Long>{

}
